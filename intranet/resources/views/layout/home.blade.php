<!DOCTYPE html>
<html lang="en">

<head>
    @include('include.head')
    @yield('style')
    <link rel="stylesheet" href="{{asset('css/pages/home.css')}}">
</head>

<body>
    @include('include.header')
    @yield('style')
    <div class="container-fluid p-0">
        <div class="row padding_body">
            @yield('content')
        </div>
    </div>
    @yield('script')

    <footer class="footer">
        @include('include.footer')
    </footer>

</body>

</html>
