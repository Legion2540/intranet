@extends('layout.home')
@section('content')


<div class="btn_popular p-0">
    <table>
        <tbody>
            <tr>
                <td colspan="2" class=" card_div_left">
                    <div class="card_div_left">
                        <img src="/img/icon/group-51.png" class="card-img-top img_left_home" alt="...">
                    </div>
                </td>

                <td colspan="2">
                    <div class="card_div_left">
                        <img src="/img/icon/group-46.png" class="card-img-top img_left_home" alt="...">
                    </div>
                </td>
            </tr>


            <tr>
                <td colspan="2">
                    <div class="card_div_left">
                        <img src="/img/icon/group-48.png" class="card-img-top img_left_home" alt="...">
                    </div>
                </td>

                <td colspan="2">
                    <div class="card_div_left">
                        <img src="/img/icon/group-47.png" class="card-img-top img_left_home" alt="...">
                    </div>
                </td>
            </tr>


            <tr>
                <td colspan="2">
                    <div class="card_div_left">
                        <img src="/img/icon/group-49.png" class="card-img-top img_left_home" alt="...">
                    </div>
                </td>

                <td colspan="2">
                    <div class="card_div_left">
                        <img src="/img/icon/group-50.png" class="card-img-top img_left_home" alt="...">
                    </div>
                </td>
            </tr>



        </tbody>
    </table>

</div>


<div class="middle_index p-0">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">
                <img src="/img/profile/107073556_3164050847020715_7388594928252935091_n.jpeg"
                    style="max-width:100%; max-height:100%;" alt="">
            </div>

            <div class="swiper-slide">
                <img src="/img/profile/176198235_168911881677682_7343545927848232125_n.jpeg"
                    style="max-width:100%; max-height:100%;" alt="">
            </div>

            <div class="swiper-slide">
                <img src="/img/profile/176223744_360115922087373_748574166255929935_n.jpeg"
                    style="max-width:100%; max-height:100%;" alt="">
            </div>

            <div class="swiper-slide">
                <img src="/img/profile/176308934_968603687281150_8398490212751100704_n.jpeg"
                    style="max-width:100%; max-height:100%;" alt="">
            </div>

            <div class="swiper-slide">
                <img src="/img/profile/176449336_294627282067968_8570528169035308806_n.jpeg"
                    style="max-width:100%; max-height:100%;" alt="">
            </div>
        </div>



        <div class="swiper-pagination"></div>
    </div>



</div>


<div class="end_index p-0 container">
    <div class="row d-flex mr-2 mt-3 ml-2">


        <div class="col-5 pr-0">
            <img src="/img/icon/tag-name.png" class="mb-2" alt="">
            <span class="ads">ประกาศ</span>
        </div>



        <div class="col-7 pl-5 text-end">
            <span class="ads text-end">
                <>
            </span>
        </div>
    </div>




    <div class="row d-flex" style="margin: 8px 50px 0px 23px">
        <span class="p-0 m-0" id="date_ads"> 22 April 2021</span>
    </div>

    <div class="row d-flex" style="margin: 0px 50px 0px 23px">
        <span class="p-0 m-0" id="title_ads">ประกาศวันหยุดตามประเพณีสำหรับปี 2564 สามารถตรวจสอบได้ทางอีเมลของท่าน</span>
    </div>

    <div class="row d-flex" style="margin:10px 50px 0px 15px">
        <span id="by">by สํานักบริหารกลาง</span>
    </div>


</div>



<div class="calendar_div p-0 border">
    <div class="row ml-2 mt-2 d-flex align-items-center text-start">
        <div class="align-items-center ">
            <img src="/img/icon/date-range-material.png" style="width:15px; height:15px; margin-bottom: 8px;" alt="">
            <span class="schedule_calendar_span">ปฏิทินกิจกรรม</span>
        </div>

    </div>


    <div class="calendar bg-primary " style="width: 100%;height: 300px;">
        {{-- Not to start --}}
    </div>


    <div class="row ml-2 my-2 d-flex align-items-center text-start">
        <div class="col-7 p-0">
            <span class="schedule_calendar_span">กิจกรรมล่าสุด</span>
        </div>
        <div class="col-5 text-center p-0">
            <img src="/img/icon/group-14-copy-2.png" alt="">
        </div>
    </div>

    <div class="swiper-container">
        <div class="swiper-wrapper mb-5">

            <div class="swiper-slide">
                <div class="container">
                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">19</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                        </div>
                    </div>
                    <div class="col-12 border-bottom px-2"></div>

                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">27-29</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">กิจกรรม CSR
                                โครงการติดตั้งโซลาเซลล์ให้กับมูลนิธิดอยสะเก็ด</span>
                        </div>
                    </div>
                    <div class="col-12 border-bottom px-2"></div>

                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">19</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="swiper-slide">

                <div class="container">
                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">19</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                        </div>
                    </div>
                    <div class="col-12 border-bottom px-2"></div>

                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">19</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                        </div>
                    </div>
                    <div class="col-12 border-bottom px-2"></div>

                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">19</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="swiper-slide">

                <div class="container">
                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">19</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                        </div>
                    </div>
                    <div class="col-12 border-bottom px-2"></div>

                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">27-29</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">กิจกรรม CSR
                                โครงการติดตั้งโซลาเซลล์ให้กับมูลนิธิดอยสะเก็ด</span>
                        </div>
                    </div>
                    <div class="col-12 border-bottom px-2"></div>

                    <div class="row mb-3">
                        <div class="col-4 pr-0">
                            <div class="row pr-0 ">
                                <div class="col-12 ">
                                    <span style="font-size: 24px">19</span>
                                </div>
                                <div class="col-12" style="line-height: 1px">
                                    <span style="font-size: 9px">JAN 21</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-8 pl-0">
                            <span style="font-size: 14px">ทำบุญบริษัท ประจำปี 2564</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="swiper-pagination"></div>
    </div>
</div>






<div class="knowledgeCenter shadow">
    <div class="row">
        <div class="col-4 pr-0">
            <span style="font-size: 26px;color: #4f72e5; font-weight: bold;">Knowledge Center</span>
        </div>

        <div class="col-4">
            <div class="dropdown">
                <button class="btn btn-light dropdown-toggle px-2 pt-2" type="button" id="dropdownMenuButton1"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    <span class="mr-5">สำนักทั้งหมด</span>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" href="#">Action</a></li>
                    <li><a class="dropdown-item" href="#">Another action</a></li>
                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
            </div>
        </div>

        <div class="col-2 pr-0"></div>

        <div class="col-2 pl-0 text-center">
            <img class="pt-1" src="/img/icon/group-14-copy-2.png" alt="">
        </div>


    </div>

    <div class="row" style="margin: 5px 0px 8px 0px">
        <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">

            <div class="logo_blue_knowledge">
                <img src="/img/icon/group-8-copy.png" alt="">
            </div>

            <div class="col-8 pl-0">
                <div class="row">
                    <div>
                        <a href="#" class="link_text" style="text-decoration: underline;">สำนักตลาดพาณิชย์ดิจิทัล</a>
                    </div>

                    <div class="content_knowledge">
                        <span>ขั้นตอนการส่งออก : กล้องถ่ายรูปและชิ้นส่วน</span>
                    </div>
                </div>
            </div>

            <div class="col-4 pt-2 pl-4">
                <div class="row by_knowledge">
                    <div class="col-12 ">
                        <span>by กฤตวิทย์ เสนาลอย</span>
                    </div>
                    <div class="col-12">
                        <span>19 ม.ค. 2563 09:00</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">
            <div class="logo_blue_knowledge">
                <img src="/img/icon/group-8-copy.png" alt="">
            </div>

            <div class="col-8 pl-0">
                <div class="row">
                    <div>
                        <a href="#" class="link_text" style="text-decoration: underline;">สำนักตลาดพาณิชย์ดิจิทัล</a>
                    </div>

                    <div class="content_knowledge">
                        <span>ขั้นตอนการส่งออก : เครื่องเงินและเครื่องประดับเงิน</span>
                    </div>
                </div>
            </div>

            <div class="col-4 pt-2 pl-4">
                <div class="row by_knowledge">
                    <div class="col-12 ">
                        <span>by กฤตวิทย์ เสนาลอย</span>
                    </div>
                    <div class="col-12">
                        <span>19 ม.ค. 2563 09:00</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">
            <div class="logo_violet_knowledge">
                <img src="/img/icon/group-18.png" alt="">
            </div>

            <div class="col-8 pl-0">
                <div class="row">
                    <div>
                        <a href="#" class="link_text" style="text-decoration: underline;">กลุ่มตรวจสอบภายใน</a>
                    </div>

                    <div class="content_knowledge">
                        <span>ขั้นตอนการเตรียมเอกสารเพื่อยื่นภาษีเงินได้ส่วนบุคคล ประจำปี 2563 </span>
                    </div>
                </div>
            </div>

            <div class="col-4 pt-2 pl-4">
                <div class="row by_knowledge">
                    <div class="col-12 ">
                        <span>by Darisa Patcharachot</span>
                    </div>
                    <div class="col-12">
                        <span>17 ม.ค. 2563 09:00</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 pl-0 d-flex mb-2 " style="background-color: #eaeaea">
            <div class="logo_pink_knowledge">
                <img src="/img/icon/group-3.png" alt="">
            </div>

            <div class="col-8 pl-0">
                <div class="row">
                    <div>
                        <a href="#" class="link_text"
                            style="text-decoration: underline;">สำนักส่งเสริมการค้าสินค้าไลฟ์สไตล์</a>
                    </div>

                    <div class="content_knowledge">
                        <span>รายละเอียดและระยะเวลาดำเนินการ โครงการ “STYLE Bangkok” งานแสดงสินค้าไลฟ์สไตล์ ประจำปี
                            2564</span>
                    </div>
                </div>
            </div>

            <div class="col-4 pt-2 pl-4">
                <div class="row by_knowledge">
                    <div class="col-12 ">
                        <span>by Chiraphon Thongthai</span>
                    </div>
                    <div class="col-12">
                        <span>16 ม.ค. 2563 09:00</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-12 pl-0 d-flex mb-2" style="background-color: #eaeaea">
            <div class="logo_blue_knowledge">
                <img src="/img/icon/group-8-copy.png" alt="">
            </div>

            <div class="col-8 pl-0">
                <div class="row">
                    <div>
                        <a href="#" class="link_text" style="text-decoration: underline;">สำนักตลาดพาณิชย์ดิจิทัล</a>
                    </div>

                    <div class="content_knowledge">
                        <span>ขั้นตอนการส่งออก : รถดับเพลิง เครื่องดับเพลิงและอุปกรณ์ดับเพลิง </span>
                    </div>
                </div>
            </div>

            <div class="col-4 pt-2 pl-4">
                <div class="row by_knowledge">
                    <div class="col-12 ">
                        <span>by กฤตวิทย์ เสนาลอย</span>
                    </div>
                    <div class="col-12">
                        <span>19 ม.ค. 2563 09:00</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 border border-bottom-2 mt-3"></div>
    </div>

    <div class="row">
        <div class="col-8">
            <span style="font-size: 26px;color: #4f72e5; font-weight: bold;">Blog</span>
        </div>

        <div class="col-2 pr-0"></div>

        <div class="col-2 pl-0 text-center">
            <img class="pt-1" src="/img/icon/group-14-copy-2.png" alt="">
        </div>

    </div>

    <div class="row" style="margin: 5px 0px 8px 0px">
        <div class="col-4 pr-0 pl-0">
            <img src="/img/icon/rectangle.png" alt="">
        </div>
        <div class="col-8 p-3" style="background-color: #eaeaea">
            <div class="row">
                <div class="col-12">
                    <span style="font-family: SukhumvitSet;
                    font-size: 20px;
                    font-weight: bold;
                    font-stretch: normal;
                    font-style: normal;
                    letter-spacing: normal;
                    color: #4a4a4a;">การล็อกดาวน์ช่วยกระตุ้นยอดสั่งอาหาร Takeaway และ Groceries ในเนเธอร์แลนด์สูงกว่า
                        50%</span>
                </div>
                <div class="col-12 mt-2">
                    <a href="#" style="text-decoration: underline">สถานการณ์การค้าผลกระทบ COVID-19</a>
                </div>
                <div class="col-12 d-flex  mt-3">
                    <div class="col-6 pl-0">
                        <span>5 ม.ค. 2564 11:00</span>
                    </div>
                    <div class="col-6 px-0">
                        <img src="/img/icon/grade-material.png" style="padding-bottom:5px" alt="">
                        <span class="rating_blog">1</span>
                        <img src="/img/icon/comment.png" alt="">
                        <span class="rating_blog">2</span>
                        <img src="/img/icon/visibility-material.png" alt="">
                        <span class="rating_blog">25</span>
                    </div>
                </div>

                <div class="col-12 d-flex mt-2">
                    <div class="col-1 px-0 d-flex">
                        <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg"
                            alt="">
                    </div>


                    <div class="col-11">
                        <div class="row mt-2" style="line-height: 1.2">
                            <div class="col-12">
                                <span style="font-size: 14px;">Wanchai Nagtang</span>
                            </div>
                            <div class="col-12">
                                <span style="font-size: 12px;
                                ">Programmer most famous</span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-12">
            <span style="font-size: 26px;color: #4f72e5; font-weight: bold;">Highlight</span>
        </div>

        <div class="col-12 d-flex">
            <div class="mr-2" style="width: fit-content; max-height:fit-content; background-color:#eaeaea">
                <div class="row">
                    <div class="col-12">
                        <img src="/img/icon/rectangle-copy-32.png" class="w-100" alt="">
                    </div>
                    <div class="col-12">
                        <div class="row p-2">
                            <div class="col-12">
                                <span class="text_highlight">จีน: ทิศทางการบริโภคอาหารทะเลในช่วงตรุษจีน 2564
                                    (สคต.เซี่ยเหมิน)</span>
                            </div>
                            <div class="col-12">
                                <a href="#" style="text-decoration: underline;font-size: 14px;">เกษตรและอาหาร</a>
                            </div>
                            <div class="col-12 d-flex">
                                <div class="col-6 pl-0">
                                    <span>31 ธ.ค. 2563 09:00</span>
                                </div>
                                <div class="col-6 px-0">
                                    <img src="/img/icon/grade-material.png" style="padding-bottom:5px" alt="">
                                    <span class="rating_highlight">1</span>
                                    <img src="/img/icon/comment.png" style="padding-bottom:5px" alt="">
                                    <span class="rating_highlight">2</span>
                                    <img src="/img/icon/visibility-material.png" style="padding-bottom:5px" alt="">
                                    <span class="rating_highlight">25</span>
                                </div>
                            </div>
                            <div class="col-12 d-flex pl-0">
                                <div class="col-2">
                                    <img id="blog_profile"
                                        src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                                </div>
                                <div class="col-10">
                                    <div class="row mt-2" style="line-height: 1.2">
                                        <div class="col-12">
                                            <span style="font-size: 14px;">Wanchai Nagtang</span>
                                        </div>
                                        <div class="col-12">
                                            <span style="font-size: 12px;
                                            ">Programmer most famous</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="mr-2" style="width: fit-content; max-height:fit-content; background-color:#eaeaea">
                <div class="row">
                    <div class="col-12">
                        <img src="/img/icon/rectangle-copy-33.png" class="w-100" alt="">
                    </div>
                    <div class="col-12">
                        <div class="row p-2">
                            <div class="col-12">
                                <span
                                    class="text_highlight">ยอดจำหน่ายอาหารกึ่งสำเร็จรูปในตลาดจีนมีแนวโน้มเติบโตเพิ่มขึ้น</span>
                            </div>
                            <div class="col-12">
                                <a href="#" style="text-decoration: underline;font-size: 14px;">เกษตรและอาหาร</a>
                            </div>
                            <div class="col-12 d-flex">
                                <div class="col-6 pl-0">
                                    <span>27 ธ.ค. 2563 09:00</span>
                                </div>
                                <div class="col-6 px-0">
                                    <img src="/img/icon/grade-material.png" style="padding-bottom:5px" alt="">
                                    <span class="rating_highlight">1</span>
                                    <img src="/img/icon/comment.png" style="padding-bottom:5px" alt="">
                                    <span class="rating_highlight">2</span>
                                    <img src="/img/icon/visibility-material.png" style="padding-bottom:5px" alt="">
                                    <span class="rating_highlight">25</span>
                                </div>
                            </div>
                            <div class="col-12 d-flex pl-0">
                                <div class="col-2">
                                    <img id="blog_profile"
                                        src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
                                </div>
                                <div class="col-10">
                                    <div class="row mt-2" style="line-height: 1.2">
                                        <div class="col-12">
                                            <span style="font-size: 14px;">Wanchai Nagtang</span>
                                        </div>
                                        <div class="col-12">
                                            <span style="font-size: 12px;
                                            ">Programmer most famous</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>




<div class="Board">
    <div class="row mb-3">
        <div class="col-8 d-flex pt-1 align-items-center">
            <span style="font-size: 24px;
            font-weight: bold;
            color: #ffffff;">Board</span>
            <div class="lineUp_Blog"></div>
            <span style="  font-size: 12px;
            font-weight: bold;color:#ffffff">การพูดคุยล่าสุด</span>
        </div>

        <div class="col-4 d-flex align-items-center" style="padding-top: 4px;">
            <img src="/img/icon/group-14.png" alt="">
        </div>
    </div>

    {{-- Card Board --}}
    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>

    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>

    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>

    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>

    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>

    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>

    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>

    <div class="row mx-auto my-2"
        style=" background-color: rgba(255, 255, 255, 0.3); padding: 6px 0px 6px 0px; border-radius: 2px;">
        <div class="col-12 d-flex justify-content-end">
            <img src="/img/icon/more-horiz-material-copy-3.png" style="vertical-align: top" alt="">
        </div>

        <div class="col-1">
            <img src="/img/icon/comment-material-copy-5.png" alt="">
        </div>
        <div class="col-10" style="line-height: 1">
            <span style="font-size: 14px;">สงกรานต์นี้เที่ยวไหนดีให้ปลอดภัย มาแชร์กันครับ</span>
        </div>


        {{-- Card Board --}}
        <div class="col-12 d-flex">
            <div class="col-2 px-0">
                <img id="blog_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </div>
            <div class="col-6 px-0 mr-3">
                <div class="row mt-2" style="line-height: 1">
                    <div class="col-12">
                        <span style="font-size: 12px;
                        font-weight: bold;">Wanchai Nagtang</span>
                    </div>
                    <div class="col-12">
                        <span style=" font-size: 10px;
                        font-weight: 600;
                        ">4 มี.ค. 2564 09:40</span>
                    </div>
                </div>
            </div>
            <div class="col-4 px-0 d-flex align-items-center justify-content-center">
                <img class="rating_Board" src="/img/icon/forum-material-copy-5.png" alt="">
                <span class="rating_Board">10</span>
                <img class="rating_Board" src="/img/icon/thumb-up-material-copy-4.png" alt="">
                <span class="rating_Board">10</span>
            </div>
        </div>

    </div>


</div>





<div class="files_popular"></div>


<div class="survay_form">
    <div class="row">

        <div class="col-12 d-flex align-content-center">
            <div class="col-9">
                <span style=" font-size: 24px;
                font-weight: bold; color: #4f72e5;">แบบสอบถาม</span>
            </div>
            <div class="col-3">
                <img src="/img/icon/group-14-copy-2.png" alt="">
            </div>
        </div>

        <div class="col-12 d-flex" style="line-height: 1.07;">
            <div class="col-1">
                <img src="/img/icon/forum-material.png" alt="">
            </div>

            <div class="col-11">
                <span style="color: #4a4a4a;  font-size: 14px;">แบบประเมินการทำงานที่บ้านในสถานการณ์โควิด 19</span>
            </div>
        </div>

        <div class="col-12 border-bottom"></div>


    </div>
</div>























<script>
    var swiper = new Swiper('.swiper-container', {
        direction: 'horizontal',
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },
    });

    var swiper_container_event = new Swiper('.calendar_div.swiper-container', {
        direction: 'horizontal',
        loop: true,
        pagination: {
            el: '.swiper-pagination',
        },
    });

</script>
@endsection
