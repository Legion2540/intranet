<style>
    .dropright {
        position: relative;
    }

    .dropright .dropdown-menu {
        top: 0;
        left: 100%;
        margin-top: -1px;
    }

    .navbar {
        height: 75px;
        background-image: linear-gradient(to bottom, #4f72e5 3%, #314d7b 180%);
    }

    ul li a.nav-link {
        color: #ffffff !important;
        font-family: SukhumvitSet;
        font-size: 14px;
        font-weight: 500;
    }

    ul li a.dropdown-item,
    button.dropdown-toggle {
        color: #4f72e5 !important;
        font-family: SukhumvitSet;
        font-size: 14px;
        font-weight: 500;
    }

    input#search,
    input#search:focus,
    input#search:active {
        width: 137px;
        fill: none;
        background: transparent;
        border: none;
        border-bottom: solid 0.5px #ffffff;
        font-size: 18px;
        margin-bottom: 16px;
        color: #ffffff;
        font-family: SukhumvitSet-Text;

    }

    input#search::placeholder {
        width: 37px;
        height: 26px;
        margin: 9px 76px 8.8px 38px;
        opacity: 0.26;
        font-family: SukhumvitSet;
        font-size: 16px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: normal;
        letter-spacing: normal;
        color: #ffffff;
    }

    #search {
        position: absolute;
    }


    #search_img {
        position: relative;
        left: 113px;

    }

    .lineUp {
        width: 1px;
        height: 26px;
        margin: 0px 0px 10px 120px;
        border: solid 0.5px #ffffff;
    }

    .lineUp2 {
        width: 1px;
        height: 26px;
        margin: 4px 0px 13px 16px;
        border: solid 0.5px #ffffff;
    }

    #nav_nameProfile {
        color: #ffffff;
    }

    #nav_profile {
        width: 40px;
        height: 40px;
        margin: 1px 0 2.8px 9px;
        object-fit: contain;
        border: solid 2px #ffffff;
        border-radius: 50%;
    }

    ul li a{
        padding-bottom: 0px !important;
        font-weight: bolder;

    }

    .layout_padding{
        padding: 0px 250px 0px 250px;
    }

    .nav-link{
        margin-right: 9px !important;
    }

</style>


<nav class="navbar navbar-expand-lg p-0 justify-content-center">
    <div class="container-fluid m-0 d-flex justify-content-center layout_padding">
        <ul class="navbar-nav align-items-center" style="width: 85%">
            <li class="nav-item mr-5 ">
                <img src="/img/logo/group-2.svg" alt="">
            </li>
            <li class="nav-item">
                <a class="nav-link" aria-current="page" href="#">หน้าหลัก</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">ข่าวสาร</a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Hot News </a></li>
                    <li><a class="dropdown-item" href="#">FactSheet</a></li>
                    <li><a class="dropdown-item" href="#">IT News</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">
                    จอง / ขอ
                </a>

                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">ระบบจองรถ</a></li>

                    <div class="dropright">
                        <button class="btn btn-light dropdown-toggle" style="padding-right:0; padding-left:23px;"
                            data-toggle="dropdown">ระบบจองห้องประชุม</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">ห้องประชุม</a>
                            <a class="dropdown-item" href="#">อาหารว่าง</a>
                            <a class="dropdown-item" href="#">Conferrence</a>
                        </div>
                    </div>


                    <li><a class="dropdown-item" href="#">คำของบประมาณ</a></li>
                    <li><a class="dropdown-item" href="#">ระบบบริหารครุภัณฑ์</a></li>
                </ul>
            </li>



            <li class="nav-item">
                <a class="nav-link" href="#">Knowledge Center</a>
            </li>


            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">Blog</a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">สถานการณ์การค้าผลกระทบ COVID-19</a></li>
                    <li><a class="dropdown-item" href="#">เกษตรและอาหาร</a></li>
                    <li><a class="dropdown-item" href="#">อุตสาหกรรม</a></li>
                    <li><a class="dropdown-item" href="#">ไลฟ์สไตล์/แฟชั่น</a></li>
                    <li><a class="dropdown-item" href="#">อัญมณีและตลาดเฉพาะ</a></li>
                    <li><a class="dropdown-item" href="#">สุขภาพและความงาม</a></li>
                    <li><a class="dropdown-item" href="#">ธุรกิจบริการ</a></li>
                </ul>
            </li>



            <li class="nav-item">
                <a class="nav-link" href="#">Board</a>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">ทำเนียบพนักงาน</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                    aria-expanded="false">คลัง</a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Photo</a></li>
                    <li><a class="dropdown-item" href="#">VDO</a></li>
                    <li><a class="dropdown-item" href="#">File</a></li>
                </ul>
            </li>

            <li class="nav-item ml-4">
                <div class="mb-2">
                    <input type="text" name="search" id="search" placeholder="ค้นหา">
                    <img id="search_img" src="/img/icon/search-material.png" />
                </div>
            </li>


            <li class="nav-item ml-2">
                    <div class="lineUp"></div>
            </li>

            <li class="nav-item ml-3 mb-2">
                <img src="/img/icon/notifications-material.png" alt="">
            </li>
            <li class="nav-item ml-3 mb-2">
                <img src="/img/icon/mail.png" alt="">
            </li>

            <li class="nav-item d-flex mr-4">
                    <div class="lineUp2"></div>
            </li>
        </ul>





        <ul class="navbar-nav align-items-center ">
            <li class="nav-item ">
                <span id="nav_nameProfile">Wanchai Nagtang</span>
            </li>


            <li class="nav-item mb-1">
                <img id="nav_profile" src="/img/icon/107073556_3164050847020715_7388594928252935091_n.jpeg" alt="">
            </li>


        </ul>
    </div>
</div>

</nav>





<script>
    $(document).ready(function () {
        $('.dropright button').on("click", function (e) {
            e.stopPropagation();
            e.preventDefault();

            if (!$(this).next('div').hasClass('show')) {
                $(this).next('div').addClass('show');
            } else {
                $(this).next('div').removeClass('show');
            }

        });
    });

</script>
